import React from "react";

type Props = {
  promise: Promise<Post[]>;
};

export default async function UserPosts({ promise }: Props) {
  const posts = await promise;
  const content = posts?.map((post) => {
    return (
      <>
        <article key={post.id}>
          <h1 className="mb-10">{post.title}</h1>
          <p>{post.body}</p>
        </article>
      </>
    );
  });
  return content
}
