import React from 'react'
import getUser from '@/lib/getUser'
import getUserPosts from '@/lib/getUserPosts'
import UserPosts from './components/UserPosts'

type Params={
    params:{
        userId:string
    }
}

export default async function UserPage({params:{userId}}:Params) {
  
const userPostData:Promise<Post[]>= getUserPosts(userId)
const userData:Promise<User> = getUser(userId);
// const [user,userPosts]= await Promise.all([userData,userPostData]);   
const user = await userData;
 
  return (
    <>
    <h1 className='mb-10'>{user.name}</h1>
     <UserPosts promise={userPostData}  />
    </>
  )
}
