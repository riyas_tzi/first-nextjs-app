import React from "react";
import type { Metadata } from "next";
import getAllUsers from "@/lib/getAllUsers";
import Link from "next/link";

export const metadata: Metadata = {
  title: "users",
};

export default async function UsersPage() {
  const userData: Promise<User[]> = getAllUsers();
  const users = await userData;
  // console.log("all users", users);

  const content = (
    <section>

      <h1 className="mb-6">
        <Link href="/">Back to home</Link>
      </h1>

      {users &&
        users?.map((user) => {
          return (
            <>
              <ul>
                <li key={user.id}>
                <Link href={`/users/${user.id}`}>{user.name}</Link>
                </li>
              </ul>
            </>
          );
        })}
    </section>
  );

  return <>{content}</>;
}
